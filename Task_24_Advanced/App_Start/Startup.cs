﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.BLL.Interfaces.Identity;
using Task_24_Advanced.BLL.Services.Identity;

[assembly: OwinStartup(typeof(Task_24_Advanced.App_Start.Startup))]

namespace Task_24_Advanced.App_Start
{
    /// <summary>
    /// Start of program
    /// </summary>
    public class Startup
    {

        readonly IServiceCreator serviceCreator = new ServiceCreator();
       /// <summary>
       /// Programm config
       /// </summary>
       /// <param name="app">OWIN service AppBuilder</param>
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<IUserService>(CreateUserService);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });
        }

        private IUserService CreateUserService()
        {
            return serviceCreator.CreateUserService("DefaultConnection");
        }
    }
}