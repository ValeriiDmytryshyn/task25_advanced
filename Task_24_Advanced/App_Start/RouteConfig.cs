﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Task_24_Advanced
{
    /// <summary>
    /// Program rout config settings
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Registr program routs
        /// </summary>
        /// <param name="routes"></param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "ArticlesByTag",
                url: "Home/ArticlesByTag/{id}/{tag}",
                defaults: new { controller = "Home", action = "ArticlesByTag", id = UrlParameter.Optional, tag = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "ArchiveSort",
                url: "Home/Archive/{id}/{sort}",
                defaults: new { controller = "Home", action = "Archive", id = UrlParameter.Optional, sort = UrlParameter.Optional }
            );
            
        }
    }
}
