﻿using System.Web.Mvc;
namespace Task_24_Advanced
{
    /// <summary>
    /// Filter registration
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Registr filter
        /// </summary>
        /// <param name="filters"></param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
