﻿using Ninject.Modules;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.BLL.Services;

namespace Task_24_Advanced.Util
{
    /// <summary>
    /// Bind intarfaces with realization
    /// </summary>
    public class NinjModule : NinjectModule
    {
        /// <summary>
        /// Override NinjModule method
        /// </summary>
        public override void Load()
        {
            Bind<IHomeService>().To<HomeService>();
            Bind<IBookService>().To<BookService>();
            Bind<IGuestService>().To<GuestService>();
            Bind<IFormService>().To<FormService>();
        }
    }
}