﻿using System.Web.Mvc;

namespace Task_24_Advanced.Helpers
{
    /// <summary>
    /// HTML Helper
    /// </summary>
    public static class RouteHelper
    {
        /// <summary>
        /// HTML Link replacement
        /// </summary>
        /// <param name="html">html</param>
        /// <param name="name">What will be display</param>
        /// <param name="path">Link to the other website etc.</param>
        /// <returns></returns>
        public static MvcHtmlString CreateRoute(this HtmlHelper html, string name, string path)
        {
            TagBuilder a = new TagBuilder("a");
            a.MergeAttribute("href", path);
            a.MergeAttribute("class", "nav-link active text-white");
            a.InnerHtml = name;

            return new MvcHtmlString(a.ToString());
        }
    }
}