﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Task_24_Advanced.Helpers
{
    /// <summary>
    /// HTML Helper
    /// </summary>
    public static class ListHelper
    {
        /// <summary>
        /// Create list of items
        /// </summary>
        /// <param name="html">html</param>
        /// <param name="items">List of strings with elements of list</param>
        /// <returns></returns>
        public static MvcHtmlString CreateList(this HtmlHelper html, List<string> items)
        {
            TagBuilder ul = new TagBuilder("ul");
            foreach (string item in items)
            {
                TagBuilder li = new TagBuilder("li");
                li.SetInnerText(item.ToUpper());
                ul.InnerHtml += li.ToString();
            }
            return new MvcHtmlString(ul.ToString());
        }
    }
}