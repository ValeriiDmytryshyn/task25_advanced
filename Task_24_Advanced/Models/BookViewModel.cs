﻿using System;
using Task_24_Advanced.BLL.DTO;

namespace Task_24_Advanced.Models
{
    /// <summary>
    /// Book model
    /// </summary>
        public class BookViewModel : BaseClassVeiwModel
        {
            /// <summary>
            /// Book title
            /// </summary>
            public string Name { get; set; }
        /// <summary>
        /// Book short description
        /// </summary>
        public string ShortDescription { get; set; }
        /// <summary>
        /// Book author full name
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Book img
        /// </summary>
        public string LinkToImg { get; set; }
        /// <summary>
        /// Book post date
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Book category
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// DTO constructor
        /// </summary>
        /// <param name="book">DTO book</param>
            public BookViewModel(BookDTO book)
            {
                Id = book.Id;
                Category = book.Category;
                Name = book.Name;
                ShortDescription = book.ShortDescription;
                Date = book.Date;
                LinkToImg = book.LinkToImg;
                Author = book.Author;
            }
        /// <summary>
        /// Empty constructor
        /// </summary>
        public BookViewModel(){}
        }
}