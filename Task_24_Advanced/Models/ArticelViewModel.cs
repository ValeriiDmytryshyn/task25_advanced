﻿using System;
using System.Collections.Generic;
using Task_24_Advanced.BLL.DTO;

namespace Task_24_Advanced.Models
{
    /// <summary>
    /// ViewModel class article
    /// </summary>
    public class ArticelViewModel : BaseClassVeiwModel
    {
        /// <summary>
        /// Article's title
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Article's category  
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// Article's post date
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Article's tags
        /// </summary>
        public List<string> Tags { get; set; } = new List<string>();
        /// <summary>
        /// Article's content
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// Article's img
        /// </summary>
        public string WayToImg { get; set; }
        /// <summary>
        /// DTO constructor
        /// </summary>
        /// <param name="article">DTO article</param>
        public ArticelViewModel(ArticleDTO article)
        {
            Id = article.Id;
            Category = article.Category;
            Name = article.Name;
            Date = article.Date;
            Content = article.Content;
            WayToImg = article.WayToImg;
        }
        /// <summary>
        /// Empty constructor
        /// </summary>
        public ArticelViewModel(){}
    }
}