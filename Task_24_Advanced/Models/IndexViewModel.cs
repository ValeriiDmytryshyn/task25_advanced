﻿using System.Collections.Generic;
namespace Task_24_Advanced.Models
{
    /// <summary>
    /// Generic method that can take list with any data
    /// </summary>
    /// <typeparam name="T">Class</typeparam>
    public class IndexViewModel<T> where T: class
    {
        /// <summary>
        /// List of items
        /// </summary>
        public List<T> Items { get; set; }
        /// <summary>
        /// Pagination
        /// </summary>
        public Pagination Pagination { get; set; }
    }
}