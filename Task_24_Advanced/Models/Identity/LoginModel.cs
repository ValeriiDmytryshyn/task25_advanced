﻿using System.ComponentModel.DataAnnotations;

namespace Task_24_Advanced.Models.Identity
{
    /// <summary>
    /// User login data
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// User's email
        /// </summary>
        [Required(ErrorMessage ="Enter your email")]
        public string Email { get; set; }
        /// <summary>
        /// User's password
        /// </summary>
        [Required(ErrorMessage = "Enter your password")]
        [DataType(DataType.Password, ErrorMessage = "Password must contains at least 1 upper letter, 1 number and length more then 6")]
        public string Password { get; set; }
    }
}