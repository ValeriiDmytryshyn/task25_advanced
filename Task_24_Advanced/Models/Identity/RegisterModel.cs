﻿using System.ComponentModel.DataAnnotations;

namespace Task_24_Advanced.Models.Identity
{
    /// <summary>
    /// Register user class
    /// </summary>
    public class RegisterModel
    {
        /// <summary>
        /// User's name
        /// </summary>
        [Required(ErrorMessage = "Enter your name")]
        public string Name { get; set; }
        /// <summary>
        /// User's email
        /// </summary>
        [Required(ErrorMessage ="Enter your email")]
        public string Email { get; set; }
        /// <summary>
        /// User's address
        /// </summary>
        [Required(ErrorMessage = "Enter your address")]
        public string Address { get; set; }
        /// <summary>
        /// User's password
        /// </summary>
        [Required(ErrorMessage = "Enter your password")]
        [DataType(DataType.Password, ErrorMessage = "Password must contains at least 1 upper letter, 1 number and length more then 6")]
        public string Password { get; set; }
        /// <summary>
        /// User's password confirm
        /// </summary>
        [Required(ErrorMessage = "Enter your password again")]
        [DataType(DataType.Password, ErrorMessage = "Password must contains at least 1 upper letter, 1 number and length more then 6")]
        [Compare("Password", ErrorMessage = "Password and confirm password various")]
        public string ConfirmPassword { get; set; }
    }
}