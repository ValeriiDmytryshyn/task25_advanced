﻿using System;
namespace Task_24_Advanced.Models
{
    /// <summary>
    /// Pagination class
    /// </summary>
    public class Pagination
    {
        /// <summary>
        /// Pages count
        /// </summary>
        public int PageNumber { get; set; } 
        /// <summary>
        /// Objects on page count
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Total object count
        /// </summary>
        public int TotalItems { get; set; }
        /// <summary>
        /// Total page count
        /// </summary>
        public int TotalPages 
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / PageSize); }
        }
    }
}