﻿namespace Task_24_Advanced.Models
{
    /// <summary>
    /// Base class with 1 prop ID
    /// </summary>
    public class BaseClassVeiwModel
    {
        /// <summary>
        /// Id of item
        /// </summary>
        public int Id { get; set; }
    }
}