﻿using System;
using Task_24_Advanced.BLL.DTO;

namespace Task_24_Advanced.Models
{
    /// <summary>
    /// Comment model
    /// </summary>
    public class CommentViewModel : BaseClassVeiwModel
    {
        /// <summary>
        /// User's name
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// User's comment content
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// Comment date
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// DTO constructor
        /// </summary>
        /// <param name="comment">DTO comment</param>
        public CommentViewModel(CommentDTO comment)
        {
            Id = comment.Id;
            Author = comment.Author;
            Content = comment.Content;
            Date = comment.Date;
        }
        /// <summary>
        /// Empty constructor
        /// </summary>
        public CommentViewModel(){}
    }
}