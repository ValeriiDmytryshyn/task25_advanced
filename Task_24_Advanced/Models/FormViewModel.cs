﻿using System.Collections.Generic;
using Task_24_Advanced.BLL.DTO;

namespace Task_24_Advanced.Models
{
    /// <summary>
    /// Form model
    /// </summary>
    public class FormViewModel : BaseClassVeiwModel
    {
        /// <summary>
        /// User's name
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// User's email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// User's age
        /// </summary>
        public int Age { get; set; }
        /// <summary>
        /// User's gender
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// User can leave this field blank
        /// </summary>
        public List<string> WhichDevice { get; set; }
        /// <summary>
        /// User can leave this field blank
        /// </summary>
        public List<string> WhichContent { get; set; }
        /// <summary>
        /// User's feedback content
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// Empty constructor
        /// </summary>
        public FormViewModel() { }
        /// <summary>
        /// DTO constructor
        /// </summary>
        /// <param name="form">DTO form</param>
        public FormViewModel(FormDTO form)
        {
            Id = form.Id;
            Author = form.Author;
            Email = form.Email;
            Age = form.Age;
            Gender = form.Gender;
            WhichDevice = form.WhichDevice;
            WhichContent = form.WhichContent;
            Content = form.Content;
        }
        /// <summary>
        /// Standart constructor
        /// </summary>
        /// <param name="id">Form id</param>
        /// <param name="author">User's name</param>
        /// <param name="email">User's email</param>
        /// <param name="age">User's age</param>
        /// <param name="gender">User's gender</param>
        /// <param name="whichDevice">User's favorites device</param>
        /// <param name="whichContent">User's favorite content</param>
        /// <param name="content">User's feedback content</param>
        public FormViewModel(int id, string author, string email, int age,
                    string gender, List<string> whichDevice,
                    List<string> whichContent, string content)
        {
            Id = id;
            Author = author;
            Email = email;
            Age = age;
            Gender = gender;
            WhichDevice = whichDevice;
            WhichContent = whichContent;
            Content = content;
        }
    }
}