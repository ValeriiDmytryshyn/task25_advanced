﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task_24_Advanced.BLL.DTO;
using Task_24_Advanced.BLL.Infrastructure;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.Filters;

namespace Task_24_Advanced.Controllers
{
    /// <summary>
    /// Admin actions controller
    /// </summary>
    [Authorize(Roles="admin")]
    public class AdminController : Controller
    {
        readonly IHomeService homeService;
        /// <summary>
        /// IHomeService initilize
        /// </summary>
        /// <param name="serv">IHomeService</param>
        public AdminController(IHomeService serv)
        {
            homeService = serv;
        }
        /// <summary>
        /// List of all articles
        /// </summary>
        /// <returns></returns>
        [LoggerFilterAttribute]
        public ActionResult ArticleList()
        {
            if (homeService.ArticleList().Any())
            {
                List<ArticleDTO> Articles = homeService.ArticleList();
                return View("ArticleList", Articles);
            }
            ViewBag.EmptyList = "No Articles in the list";
            return View("AddArticle");
        }
        /// <summary>
        /// Edit article by id
        /// </summary>
        /// <param name="id">Article's id</param>
        /// <returns>EditArticle view</returns>
        [HttpGet]
        public ActionResult EditArticle(string id = "1")
        {
            ArticleDTO Article;
            if (int.TryParse(id, out int Id))
            {
                Article = homeService.GetArticleById(Id);
                return View("EditArticle", Article);
            }
            Article = homeService.GetArticleById(1);
            return View("EditArticle", Article);
        }
        /// <summary>
        /// Edit article
        /// </summary>
        /// <param name="id">Article's id</param>
        /// <param name="title">New article's title</param>
        /// <param name="wayToImg">New article's img</param>
        /// <param name="content">New article's content</param>
        /// <param name="tags">New article's tags</param>
        /// <param name="date">New article's date</param>
        /// <returns>EditArticle view</returns>
        [HttpPost]
        public ActionResult EditArticle(string id, string title, HttpPostedFileBase wayToImg, string content, string tags, string date)
        {
            if (int.TryParse(id, out int ID))
            {
                try
                {
                    if (string.IsNullOrEmpty(title))
                    { throw new ValidationException("Title must contain at least 1 char", "Title"); }
                    if (string.IsNullOrEmpty(content))
                    { throw new ValidationException("Article's content must contain at least 1 char", "Content"); }
                    if (string.IsNullOrEmpty(tags))
                    { throw new ValidationException("Article's tags must contain at least 1 char", "Tags"); }
                    if (string.IsNullOrEmpty(date))
                    { throw new ValidationException("You does not enter date", "Date"); }
                    var article = homeService.GetArticleById(ID);
                    if (wayToImg != null)
                    {
                        string fileName = System.IO.Path.GetFileName(wayToImg.FileName);
                        wayToImg.SaveAs(Server.MapPath("/Content/img/" + fileName));
                        article.WayToImg = "/Content/img/" + fileName;
                    }

                    homeService.DeleteById(ID);

                    string[] TagList = tags.Split(';');
                    article.Name = title;
                    article.Content = content;
                    if (!TagList.Any()) { TagList.ToList().Add("Mini article"); }
                    article.Date = new DateTime(int.Parse(date.Split('-')[0]), int.Parse(date.Split('-')[1]),
                            int.Parse(date.Split('-')[2]), DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

                    foreach (var item in TagList)
                    {
                        if (!article.Tags.Select(x => x.Tag).Contains(item) && item != "")
                        {
                            article.Tags.Add(new TagClassDTO(item));
                        }
                    }
                    homeService.AddArticle(article);
                    homeService.Save();

                    return ArticleList();
                }
                catch (ValidationException ex)
                {
                    ModelState.AddModelError(ex.Property, ex.Message);
                }
            }
            else
            {
                ID = 1;
                ArticleDTO Article = homeService.GetArticleById(ID);
                return View("EditArticle", Article);
            }
            return ArticleList();

        }
        /// <summary>
        /// Delete article by id
        /// </summary>
        /// <param name="id">Article's id</param>
        /// <returns>DeleteArticle view</returns>
        [HttpGet]
        public ActionResult DeleteArticle(string id ="1")
        {
            var article = homeService.GetArticleById(int.Parse(id));

            return View("DeleteArticle", article);
        }
        /// <summary>
        /// Delete article by id 
        /// </summary>
        /// <param name="answer">Question was delete or no</param>
        /// <param name="id">Article's id</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteArticle(string answer, string id = "1")
        {
            if (!int.TryParse(id, out int Id))
            { throw new ValidationException("Id must be number", "ID"); }

            if (answer.ToLower() == "yes")
            {
                homeService.DeleteById(Id);
                homeService.Save();
            }
            List<ArticleDTO> Articles = homeService.ArticleList();
            return View("ArticleList", Articles);
        }
        /// <summary>
        /// Add new article
        /// </summary>
        /// <returns>AddArticle view</returns>
        [HttpGet]
        public ActionResult AddArticle()
        {
            return View("AddArticle", new ArticleDTO());    
        }
        /// <summary>
        /// Add new article
        /// </summary>
        /// <param name="Name">Article's title</param>
        /// <param name="Category">Article's category</param>
        /// <param name="Content">Article's content</param>
        /// <param name="WayToImg">Article's img</param>
        /// <param name="tags">Article's tags</param>
        /// <param name="date">Article's date</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddArticle(string Name, string Category,
            string Content, string WayToImg, string tags, string date)
        {
            try
            {
                if (string.IsNullOrEmpty(Name))
                { throw new ValidationException("Title must contain at least 1 char", "Name"); }
                if (string.IsNullOrEmpty(Category))
                { throw new ValidationException("Article's category must contain at least 1 char", "Category"); }
                if (string.IsNullOrEmpty(Content))
                { throw new ValidationException("Article's content must contain at least 1 char", "Content"); }
                if (string.IsNullOrEmpty(tags))
                { throw new ValidationException("Article's tags must contain at least 1 char", "Tags"); }
                if (string.IsNullOrEmpty(date))
                { throw new ValidationException("You does not enter date", "Date"); }

                var article = new ArticleDTO(homeService.ArticleList().Count(), Category, Name,
                    DateTime.Now, Content, WayToImg, new List<TagClassDTO>());

                string[] TagList = tags.Split(';');
                DateTime Date = new DateTime(int.Parse(date.Split('-')[0]), int.Parse(date.Split('-')[1]),
                        int.Parse(date.Split('-')[2]), DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                if (Date > DateTime.Now)
                    { throw new ValidationException("You can not enter date, that more than today", "Date"); }
                article.Date = Date;
                
                foreach (var item in TagList)
                {
                    if (item != "" && item != " ")
                    {
                        article.Tags.Add(new TagClassDTO(item));
                    }
                }
                homeService.AddArticle(article);
                homeService.Save();
                return ArticleList();
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            ArticleDTO articel = new ArticleDTO();
            if (!string.IsNullOrEmpty(Name)){ articel.Name = Name; }
            if (!string.IsNullOrEmpty(Category)){ articel.Category = Category; }
            if (!string.IsNullOrEmpty(Content)){ articel.Content = Content; }
            return View("AddArticle", articel);
        }
    }
}