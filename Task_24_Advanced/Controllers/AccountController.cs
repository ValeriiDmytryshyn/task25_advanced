﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Task_24_Advanced.BLL.DTO;
using Task_24_Advanced.BLL.Infrastructure;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.Models.Identity;

namespace Task_24_Advanced.Controllers
{
    /// <summary>
    /// Account controller
    /// </summary>
    public class AccountController : Controller
    {
        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        /// <summary>
        /// User login
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            return View();
        }
        /// <summary>
        /// User login
        /// </summary>
        /// <param name="model">Login data</param>
        /// <returns>Login view</returns>
        [HttpPost]
        public async Task<ActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO { Email = model.Email, Password = model.Password };
                ClaimsIdentity claim = await UserService.Authenticate(userDto);
                if (claim == null)
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(model);
        }
        /// <summary>
        /// Logout from account
        /// </summary>
        /// <returns>Logout view</returns>
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }
        /// <summary>
        /// Register new user view
        /// </summary>
        /// <returns>Register view</returns>
        public ActionResult Register()
        {
            return View();
        }
        /// <summary>
        /// Register new user
        /// </summary>
        /// <param name="model">User's data</param>
        /// <returns>Register view</returns>
        [HttpPost]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO
                {
                    Email = model.Email,
                    Password = model.Password,
                    Address = model.Address,
                    Name = model.Name,
                    Role = "user"
                };
                try
                {
                    await UserService.Create(userDto);
                    return RedirectToAction("Index", "Home");

                }
                catch (ValidationException ex)
                {
                    ModelState.AddModelError(ex.Message, ex.Property);
                }
            }
            return View(model);
        }
    }
}