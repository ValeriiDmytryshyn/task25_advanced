﻿using System.Collections.Generic;
using System.Web.Mvc;
using Task_24_Advanced.BLL.DTO;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.Models;

namespace Task_24_Advanced.Controllers
{
    /// <summary>
    ///  Book controlle class
    /// </summary>
    public class BookController : Controller
    {
        readonly IBookService bookService;
        /// <summary>
        /// Initilize IBookService  
        /// </summary>
        /// <param name="serv">IBookService</param>
        public BookController(IBookService serv)
        {
            bookService = serv;
        }
        /// <summary>
        /// Book's View
        /// </summary>
        /// <returns>List of all books</returns>
        public ActionResult Index()
        {
            List<BookDTO> Books = bookService.BookList();
            List<BookViewModel> outList = new List<BookViewModel>();
            foreach (var item in Books)
            {
                outList.Add(new BookViewModel(item));
            }
            return View("Index", outList);
        }
        /// <summary>
        /// Show current book by id
        /// </summary>
        /// <param name="id">Book id</param>
        /// <returns>Book view</returns>
        public ActionResult ShowBook(string id)
        {
            if (int.TryParse(id, out int Id))
            {
                var book = bookService.GetBookById(Id);
                if (book != null)
                {
                    return View("ShowBook", book);
                }
            }
            return Index();
        }
    }
}