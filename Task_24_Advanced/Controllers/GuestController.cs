﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Task_24_Advanced.BLL.DTO;
using Task_24_Advanced.BLL.Infrastructure;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.Models;

namespace Task_24_Advanced.Controllers
{
    /// <summary>
    /// Guset controller class
    /// </summary>
    public class GuestController : Controller
    {
        readonly IGuestService guestService;
        /// <summary>
        /// Initilize IGuestService  
        /// </summary>
        /// <param name="serv">IGuestService</param>
        public GuestController(IGuestService serv)
        {
            guestService = serv;
        }
        /// <summary>
        /// Return guest view with 8 last comments
        /// </summary>
        /// <returns>Guest view with 8 last comments</returns>
        [HttpGet]
        public ActionResult Index(string id = "1")
        {
            List<CommentDTO> outList = guestService.CommentList();
            int.TryParse(id, out int page);
            int pageSize = 4;
            List<CommentDTO> comments = outList.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            Pagination pagination = new Pagination { PageNumber = page, PageSize = pageSize, TotalItems = outList.Count };
            IndexViewModel<CommentDTO> CommentViewModel = new IndexViewModel<CommentDTO> { Pagination = pagination, Items = comments };

            return View("Index", CommentViewModel);
        }
        /// <summary>
        /// Take user's data. If it valide show Guest view with user's comment, else show valide message.
        /// </summary>
        /// <param name="comment">User's data</param>
        /// <param name="id">Page number</param>
        /// <returns>If user's data are valide show Guest view with user's comment, else show valide message</returns>
        [HttpPost]
        public ActionResult Index(CommentViewModel comment, string id ="1")
        {
                List<CommentDTO> outList = guestService.CommentList();
            int.TryParse(id, out int page);
            int pageSize = 4;
            List<CommentDTO> comments = outList.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            Pagination pagination = new Pagination { PageNumber = page, PageSize = pageSize, TotalItems = outList.Count };

            IndexViewModel<CommentDTO> ModelCommentViewModel = new IndexViewModel<CommentDTO> { Pagination = pagination, Items = comments };
            try
            {
                guestService.CreateComment(new BLL.DTO.CommentDTO(comment.Id, comment.Author, comment.Content, comment.Date));
                guestService.Save();

                return View("Index", ModelCommentViewModel);
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }

                
                return View("Index", ModelCommentViewModel);
        }
    }
}