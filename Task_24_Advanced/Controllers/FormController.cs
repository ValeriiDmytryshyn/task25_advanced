﻿using System.Web.Mvc;
using Task_24_Advanced.BLL.Infrastructure;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.Models;

namespace Task_24_Advanced.Controllers
{
    /// <summary>
    /// Form controlle class
    /// </summary>
    public class FormController : Controller
    {
        readonly IFormService formService;
        /// <summary>
        /// Initilize IFormService  
        /// </summary>
        /// <param name="serv">IFormService</param>
        public FormController(IFormService serv)
        {
            formService = serv;
        }
        /// <summary>
        /// Return form view
        /// </summary>
        /// <returns>Form view</returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View("Index");
        }
        /// <summary>
        /// Take user's data. If it valide show FormResult view, else show valide message.
        /// </summary>
        /// <param name="form">User's data</param>
        /// <returns>If user's data are valide show FormResult view, else show valide message</returns>
        [HttpPost]
        public ActionResult Index(FormViewModel form)
        {
            try
            {
                formService.CreateForm(new BLL.DTO.FormDTO(form.Id, form.Author, form.Email, form.Age, form.Gender, form.WhichDevice,
                form.WhichContent, form.Content));
                formService.Save();
                return View("FormResult", new FormViewModel(formService.ShowForm()));
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return View("Index");
            
        }
        /// <summary>
        /// Form with user's data
        /// </summary>
        /// <returns></returns>
        public ActionResult FormResult()
        {
             return View("FormResult");
        }
    }
}