﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Task_24_Advanced.BLL.DTO;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.Models;

namespace Task_24_Advanced.Controllers
{
    /// <summary>
    /// Home controlle class
    /// </summary>
    public class HomeController : Controller
    {
        readonly IHomeService homeService;
        /// <summary>
        /// Initilize IHomeService  
        /// </summary>
        /// <param name="serv">IHomeService</param>
        public HomeController (IHomeService serv)
        {
            homeService = serv;
        }
        /// <summary>
        /// Throw to the view list of all articles
        /// </summary>
        /// <returns>List of all articles</returns>
        public ActionResult Index()
        {
            List<ArticleDTO> Articles = homeService.ArticleList();
            List<ArticelViewModel> outList = new List<ArticelViewModel>();
            foreach (var item in Articles)
            {
                outList.Add(new ArticelViewModel(item));
            }
            return View("Index", outList);
        }
        /// <summary>
        /// Show current article
        /// </summary>
        /// <param name="id">Article's id</param>
        /// <returns>ShowArticle view</returns>
        public ActionResult ShowArticle(string id)
        {
            if (int.TryParse(id, out int Id))
            {
                var article = homeService.GetArticleById(Id);
                if (article != null)
                { 
                    return View("ShowArticle", article);
                }
            }

            return Index();
        }
        /// <summary>
        /// Article archive
        /// </summary>
        /// <param name="id">Page number</param>
        /// <param name="sort">Sort method</param>
        /// <returns>Archive with articles</returns>
        public ActionResult Archive(string id = "1", string sort = "")
        {
            List<ArticleDTO> Articles;
            if (sort.ToLower() == "fromoldtonew")
            {
                Articles = homeService.ArticleList().OrderBy(x => x.Date).ToList();
            }
            else if (sort.ToLower() == "fromnewtoold")
            {
                Articles = homeService.ArticleList().OrderByDescending(x => x.Date).ToList();
            }
            else
            {
                Articles = homeService.ArticleList();
            }
            int.TryParse(id, out int page);
            int pageSize = 5;
            List<ArticleDTO> articles = Articles.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            Pagination pagination = new Pagination { PageNumber = page, PageSize = pageSize, TotalItems = Articles.Count};
            IndexViewModel<ArticleDTO> ArticleViewModel = new IndexViewModel<ArticleDTO>{ Pagination = pagination, Items = articles };
            ViewBag.Sort = sort;
            return View("Archive", ArticleViewModel);
        }
        /// <summary>
        /// List of articles by one tag
        /// </summary>
        /// <param name="id">Page number</param>
        /// <param name="tag">Tag name</param>
        /// <returns>ArticlesByTag view</returns>
        public ActionResult ArticlesByTag(string id = "1", string tag = "Mini article")
        {
            ViewBag.Tag = tag;
            List<ArticleDTO> Articles = homeService.ArticlesByTag(tag);
            int.TryParse(id, out int page);
            int pageSize = 5;
            List<ArticleDTO> articles = Articles.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            Pagination pagination = new Pagination { PageNumber = page, PageSize = pageSize, TotalItems = Articles.Count };
            IndexViewModel<ArticleDTO> ArticleViewModel = new IndexViewModel<ArticleDTO> { Pagination = pagination, Items = articles };

            return View("ArticlesByTag", ArticleViewModel);
        }
    }
}