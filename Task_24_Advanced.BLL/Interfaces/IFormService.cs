﻿using Task_24_Advanced.BLL.DTO;

namespace Task_24_Advanced.BLL.Interfaces
{
    /// <summary>
    /// Form service intarface
    /// </summary>
    public interface IFormService
    {
        /// <summary>
        /// Show last user's form 
        /// </summary>
        /// <returns>Last user's form</returns>
        FormDTO ShowForm();
        /// <summary>
        /// Chack if user's data are valide add new form in db, else throw ValidationException
        /// </summary>
        /// <param name="form">User's data</param>
        void CreateForm(FormDTO form);
        /// <summary>
        /// Save changes in db
        /// </summary>
        void Save();
        /// <summary>
        /// Dispose data
        /// </summary>
        void Dispose();
    }
}
