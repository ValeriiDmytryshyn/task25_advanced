﻿using System.Collections.Generic;
using Task_24_Advanced.BLL.DTO;

namespace Task_24_Advanced.BLL.Interfaces
{
    /// <summary>
    /// Book service intarface
    /// </summary>
    public interface IBookService
    {
        /// <summary>
        /// List of books
        /// </summary>
        /// <returns>List of books</returns>
        List<BookDTO> BookList();
        /// <summary>
        /// Return list of books in one category
        /// </summary>
        /// <param name="categoryName">Name of books category</param>
        /// <returns>List of books in one category</returns>
        List<BookDTO> CategoryBook(string categoryName);
        /// <summary>
        /// Add book to db
        /// </summary>
        /// <param name="book">Book</param>
        void AddBook(BookDTO book);
        /// <summary>
        /// Get book from db by id
        /// </summary>
        /// <param name="id">book's id</param>
        /// <returns>Books DTO</returns>
        BookDTO GetBookById(int id);
    }
}
