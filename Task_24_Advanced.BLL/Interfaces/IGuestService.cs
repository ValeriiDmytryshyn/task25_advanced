﻿using System.Collections.Generic;
using Task_24_Advanced.BLL.DTO;

namespace Task_24_Advanced.BLL.Interfaces
{
    /// <summary>
    /// Guest serice intarface
    /// </summary>
    public interface IGuestService
    {
        /// <summary>
        /// Chack if user's data are valide add new comment in db
        /// </summary>
        /// <param name="comment">User's data</param>
        void CreateComment(CommentDTO comment);
        /// <summary>
        /// Looking comment in db for id
        /// </summary>
        /// <param name="Id">Comment id</param>
        /// <returns>Comment</returns>
        CommentDTO ShowComment(int Id);
        /// <summary>
        /// List of all comments
        /// </summary>
        /// <returns>List of comments</returns>
        List<CommentDTO> CommentList();
        /// <summary>
        /// Dispose data
        /// </summary>
        void Dispose();
        /// <summary>
        /// Save changes in db
        /// </summary>
        void Save();
    }
}