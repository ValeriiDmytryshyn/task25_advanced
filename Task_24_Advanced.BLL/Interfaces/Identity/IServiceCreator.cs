﻿namespace Task_24_Advanced.BLL.Interfaces.Identity
{
    /// <summary>
    /// Replacement ninject
    /// </summary>
    public interface IServiceCreator
    {
        /// <summary>
        /// Throw connection string
        /// </summary>
        /// <param name="connection">Connection string</param>
        /// <returns></returns>
        IUserService CreateUserService(string connection);
    }
}