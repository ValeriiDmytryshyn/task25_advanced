﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Task_24_Advanced.BLL.DTO;
using Task_24_Advanced.BLL.Infrastructure;

namespace Task_24_Advanced.BLL.Interfaces
{
    /// <summary>
    /// Interface user service
    /// </summary>
    public interface IUserService : IDisposable
    {
        /// <summary>
        /// Standatr CRUD add to db
        /// </summary>
        /// <param name="userDto">User that we are add to db</param>
        /// <returns></returns>
        Task Create(UserDTO userDto);
        /// <summary>
        /// User authentication
        /// </summary>
        /// <param name="userDto">User that we try to authenticate</param>
        /// <returns>Object ClaimsIdentity</returns>
        Task<ClaimsIdentity> Authenticate(UserDTO userDto);
    }
}