﻿using System.Collections.Generic;
using Task_24_Advanced.BLL.DTO;

namespace Task_24_Advanced.BLL.Interfaces
{
    /// <summary>
    /// Home service
    /// </summary>
    public interface IHomeService
    {
        /// <summary>
        /// List of articles in one category
        /// </summary>
        /// <param name="id">id of article</param>
        /// <returns>List of articles in one category</returns>
        ArticleDTO GetArticleById(int id);
        /// <summary>
        /// List of articles in one category
        /// </summary>
        /// <param name="TagName">Name of article's category</param>
        /// <returns>List of articles in one category</returns>
        List<ArticleDTO> ArticlesByTag(string TagName);
        /// <summary>
        /// Return list of all comments
        /// </summary>
        /// <returns>List of all comments</returns>
        List<ArticleDTO> ArticleList();
        /// <summary>
        /// Delete article from db
        /// </summary>
        /// <param name="id">article's id</param>
        void DeleteById(int id);
        /// <summary>
        /// Add article to db
        /// </summary>
        /// <param name="article">Article that you want to add</param>
        void AddArticle(ArticleDTO article);
        /// <summary>
        /// Update article in db
        /// </summary>
        /// <param name="article">Article that you want to update</param>
        void UpdateArticle(ArticleDTO article);
        /// <summary>
        /// Save changes in db
        /// </summary>
        void Save();
    }
}
