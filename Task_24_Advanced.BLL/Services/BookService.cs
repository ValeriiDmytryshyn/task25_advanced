﻿using System.Collections.Generic;
using System.Linq;
using Task_24_Advanced.BLL.DTO;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.BLL.Services
{
    /// <summary>
    /// Book service class
    /// </summary>
    public class BookService : IBookService
    {
        IUnitOfWork Database { get; set; }
        /// <summary>
        /// Initilize Unit of work  
        /// </summary>
        /// <param name="uow">UnitOfWork</param>
        public BookService(IUnitOfWork uow)
        {
            Database = uow;
        }
        /// <summary>
        /// Return list of books in one category
        /// </summary>
        /// <param name="categoryName">Name of books category</param>
        /// <returns>List of books in one category</returns>
        public List<BookDTO> CategoryBook(string categoryName)
        {
            var list = Database.Books.ListItems().Where(book => book.Category == categoryName).ToList();
            List<BookDTO> outList = new List<BookDTO>();
            foreach (var item in list)
            {
                outList.Add(new BookDTO(item));
            }
            return outList;
        }

        /// <summary>
        /// Add book to db
        /// </summary>
        /// <param name="book">Book</param>
        public void AddBook(BookDTO book)
        {
            Database.Books.Create(new Book(Database.Books.Count(), book.Category, book.Name, book.ShortDescription,
                book.Date, book.LinkToImg, book.Author));
            Database.Save();
        }
        /// <summary>
        /// List of books
        /// </summary>
        /// <returns>List of books</returns>
        public List<BookDTO> BookList()
        {
            var list = Database.Books.ListItems();
            List<BookDTO> outList = new List<BookDTO>();
            foreach (var item in list)
            {
                outList.Add(new BookDTO(item));
            }
            return outList;
        }
        /// <summary>
        /// Get book from db by id
        /// </summary>
        /// <param name="id">book's id</param>
        /// <returns>Books DTO</returns>
        public BookDTO GetBookById(int id)
        {
            if (Database.Books.GetItem(id) != null)
            {
                return new BookDTO(Database.Books.GetItem(id));
            }
            return null;
        }
    }
}