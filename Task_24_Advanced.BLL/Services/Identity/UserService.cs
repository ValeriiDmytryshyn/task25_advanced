﻿using Microsoft.AspNet.Identity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Task_24_Advanced.BLL.DTO;
using Task_24_Advanced.BLL.Infrastructure;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.DAL.Entities.Identity;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.BLL.Services.Identity
{
    /// <summary>
    /// User service class
    /// </summary>
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }

        /// <summary>
        /// Unit of work initilize 
        /// </summary>
        /// <param name="uow"></param>
    public UserService(IUnitOfWork uow)
    {
        Database = uow;
    }
        /// <summary>
        /// Standatr CRUD add to db
        /// </summary>
        /// <param name="userDto">User that we are add to db</param>
        /// <returns></returns>
        public async Task Create(UserDTO userDto)
    {
        ApplicationUser user = await Database.UserManager.FindByEmailAsync(userDto.Email);
        if (user == null)
        {
            user = new ApplicationUser { Email = userDto.Email, UserName = userDto.Email };
            var result = await Database.UserManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Any())
                    throw new ValidationException($"{result.Errors.FirstOrDefault()}");
            await Database.UserManager.AddToRoleAsync(user.Id, userDto.Role);
            ClientProfile clientProfile = new ClientProfile { Id = user.Id, Address = userDto.Address, Name = userDto.Name };
            Database.ClientManager.Create(clientProfile);
            Database.Save();
        }
        else
        {
            throw new ValidationException($"User with this email({user.Email}) already exist");
        }
    }
        /// <summary>
        /// User authentication
        /// </summary>
        /// <param name="userDto">User that we try to authenticate</param>
        /// <returns>Object ClaimsIdentity</returns>
        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
    {
        ClaimsIdentity claim = null;
        ApplicationUser user = await Database.UserManager.FindAsync(userDto.Email, userDto.Password);
        if (user != null)
            claim = await Database.UserManager.CreateIdentityAsync(user,
                                        DefaultAuthenticationTypes.ApplicationCookie);
        return claim;
    }

        /// <summary>
        /// Dispose data
        /// </summary>
        public void Dispose()
        {
            Database.Dispose();
        }
    }   
}