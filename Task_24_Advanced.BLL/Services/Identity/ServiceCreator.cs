﻿using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.BLL.Interfaces.Identity;
using Task_24_Advanced.DAL.Repositories;
namespace Task_24_Advanced.BLL.Services.Identity
{
    /// <summary>
    /// Replacement ninject
    /// </summary>
    public class ServiceCreator : IServiceCreator
    {
        /// <summary>
        /// Throw connection string
        /// </summary>
        /// <param name="connection">Connection string</param>
        /// <returns></returns>
        public IUserService CreateUserService(string connection)
        {
            return new UserService(new EFUnitOfWork(connection));
        }
    }
}