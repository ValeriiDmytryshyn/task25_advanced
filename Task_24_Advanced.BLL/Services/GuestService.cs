﻿using System;
using System.Collections.Generic;
using Task_24_Advanced.BLL.DTO;
using Task_24_Advanced.BLL.Infrastructure;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.BLL.Services
{
    /// <summary>
    /// Guset service
    /// </summary>
    public class GuestService : IGuestService
    {
        IUnitOfWork Database { get; set; }
        /// <summary>
        /// Initilize Unit of work  
        /// </summary>
        /// <param name="uow">UnitOfWork</param>
        public GuestService(IUnitOfWork uow)
        {
            Database = uow;
        }
        /// <summary>
        /// Chack if user's data are valide add new comment in db
        /// </summary>
        /// <param name="comment">User's data</param>
        public void CreateComment(CommentDTO comment)
        {
            if (comment == null)
            {
                throw new ValidationException("Please enter currect data","");
            }
            if (string.IsNullOrEmpty(comment.Author))
            {
                throw new ValidationException("Please enter currect data", "Author");
            }
            if (string.IsNullOrEmpty(comment.Content))
            {
                throw new ValidationException("Please enter currect data", "Content");
            }
            var outComment = new Comment(Database.Comments.Count(), comment.Author, comment.Content, DateTime.Now); 
            Database.Comments.Create(outComment);
            Database.Save();
        }
        /// <summary>
        /// Looking comment in db for id
        /// </summary>
        /// <param name="Id">Comment id</param>
        /// <returns>Comment</returns>
        public CommentDTO ShowComment(int Id)
        {
            var outComment = new CommentDTO(Id, Database.Comments.GetItem(Id).Author, Database.Comments.GetItem(Id).Content, DateTime.Now);
            return outComment;
        }
        /// <summary>
        /// Dispose data
        /// </summary>
        public void Dispose()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// List of all comments
        /// </summary>
        /// <returns>List of comments</returns>
        public List<CommentDTO> CommentList()
        {
            List<CommentDTO> outList = new List<CommentDTO>();
            foreach (var item in Database.Comments.ListItems())
            {
                outList.Add(new CommentDTO(item.Id, item.Author, item.Content, item.Date));
            }
            return outList;
        }
        /// <summary>
        /// Save changes in db
        /// </summary>
        public void Save() {
            Database.Save();
        }
    }
}