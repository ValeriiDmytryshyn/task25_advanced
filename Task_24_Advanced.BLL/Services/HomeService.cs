﻿using System.Collections.Generic;
using System.Linq;
using Task_24_Advanced.BLL.DTO;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.BLL.Services
{
    /// <summary>
    /// Home service
    /// </summary>
    public class HomeService : IHomeService
    {
        IUnitOfWork Database { get; set; }
        /// <summary>
        /// Initilize Unit of work  
        /// </summary>
        /// <param name="uow">UnitOfWork</param>
        public HomeService(IUnitOfWork uow)
        {
            Database = uow;
        }

        /// <summary>
        /// List of articles in one category
        /// </summary>
        /// <param name="id">id of article</param>
        /// <returns>List of articles in one category</returns>
        public ArticleDTO GetArticleById(int id)
        {
            if (Database.Articles.GetItem(id) != null)
            {
                var article = new ArticleDTO(Database.Articles.GetItem(id));
                return article;
            }
            return null;
        }
        /// <summary>
        /// List of articles in one category
        /// </summary>
        /// <param name="TagName">Name of article's category</param>
        /// <returns>List of articles in one category</returns>
        public List<ArticleDTO> ArticlesByTag(string TagName)
        {
            var list = Database.Articles.ListItems().Where(article => article.Tags.Any(x => x.Tag == TagName)).ToList();
            List<ArticleDTO> outList = new List<ArticleDTO>();
            foreach (var item in list)
            {
                outList.Add(new ArticleDTO(item));
            }
            return outList;
        }
        /// <summary>
        /// Return list of all comments
        /// </summary>
        /// <returns>List of all comments</returns>
        public List<ArticleDTO> ArticleList()
        {
            List<ArticleDTO> outList = new List<ArticleDTO>();
            foreach (var item in Database.Articles.ListItems())
            {
                outList.Add(new ArticleDTO(item));
            }
            return outList;
        }
        /// <summary>
        /// Delete article from db
        /// </summary>
        /// <param name="id">Article's id</param>
        public void DeleteById(int id)
        {
            Database.Articles.Delate(id);
        }
        /// <summary>
        /// Add article to db
        /// </summary>
        /// <param name="article">Article that you want to add</param>
        public void AddArticle(ArticleDTO article) 
        {
            var articleOut = new Article(article.Id, article.Category, article.Name, article.Date, article.Content, article.WayToImg, new List<TagClass>());
            if (article.Tags != null)
            {
                foreach (var item in article.Tags)
                {
                    articleOut.Tags.Add(new TagClass(item.Id, item.Tag));
                }
            }
            Database.Articles.Create(articleOut); 
        }
        /// <summary>
        /// Update article in db
        /// </summary>
        /// <param name="article">Article that you want to update</param>
        public void UpdateArticle(ArticleDTO article)
        {
            var articleOut = new Article(article.Id, article.Category, article.Name, article.Date, article.Content, article.WayToImg, new List<TagClass>());
            foreach (var item in article.Tags)
            {
                articleOut.Tags.Add(new TagClass(item.Id, item.Tag));
            }
            Database.Articles.Update(articleOut);
        }
        /// <summary>
        /// Save changes in db
        /// </summary>
        public void Save() 
        {
            Database.Save();
        }
    }
}