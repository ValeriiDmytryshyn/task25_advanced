﻿using System;
using System.Linq;
using Task_24_Advanced.BLL.DTO;
using Task_24_Advanced.BLL.Infrastructure;
using Task_24_Advanced.BLL.Interfaces;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.BLL.Services
{
    /// <summary>
    /// Form service class
    /// </summary>
    public class FormService : IFormService
    {
        IUnitOfWork Database { get; set; }
        /// <summary>
        /// Initilize Unit of work  
        /// </summary>
        /// <param name="uow">UnitOfWork</param>
        public FormService(IUnitOfWork uow)
        {
            Database = uow;
        }
       /// <summary>
       /// Dispose data
       /// </summary>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Show last user's form 
        /// </summary>
        /// <returns>Last user's form</returns>
        public FormDTO ShowForm()
        {
            var form = Database.Forms.ListItems().Last();
            return new FormDTO(Database.Forms.Count(), form.Author, form.Email,
                form.Age, form.Gender, form.WhichDevice, form.WhichContent, form.Content);
        }

        /// <summary>
        /// Chack if user's data are valide add new form in db, else throw ValidationException
        /// </summary>
        /// <param name="form">User's data</param>
        public void CreateForm(FormDTO form)
        {
            if (form.Author == null || form.Author == "")
            { throw new ValidationException("Name must be more then 1 char", "Author"); }

            if (form.Age < 1 || form.Age > 124)
            { throw new ValidationException("Age cann't be less then 1 and more then 124", "Age"); }
            if (form.Email == null || form.Email == "")
            { throw new ValidationException("Email must looks like example@email.com", "Email"); }
            if (String.IsNullOrEmpty(form.Content))
            { throw new ValidationException("Please write your experience of using our site", "Content"); }

                Database.Forms.Create(new Form(Database.Forms.Count(), form.Author, form.Email,
                form.Age, form.Gender, form.WhichDevice, form.WhichContent, form.Content));
                Database.Save();
        }

        /// <summary>
        /// Save changes in db
        /// </summary>
        public void Save()
        {
            Database.Save();
        }
    }
}