﻿using System;

namespace Task_24_Advanced.BLL.Infrastructure
{
    /// <summary>
    /// Validation exception class
    /// </summary>
    [Serializable]
    public class ValidationException : Exception
    {
        /// <summary>
        /// Property that are center of exception
        /// </summary>
        public string Property { get; protected set; }
        /// <summary>
        /// Standart exception connstructor
        /// </summary>
        /// <param name="message">Messege of exception</param>
        /// <param name="prop">Property that are center of exception</param>
        public ValidationException(string message, string prop) : base(message)
        {
            Property = prop;
        }
        /// <summary>
        /// Standart exception connstructor
        /// </summary>
        /// <param name="message">Messege of exception</param>
        public ValidationException(string message) { }
        /// <summary>
        /// Standart exception connstructor
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected ValidationException(System.Runtime.Serialization.SerializationInfo info,
           System.Runtime.Serialization.StreamingContext context) : base(info, context) { }

    }
}