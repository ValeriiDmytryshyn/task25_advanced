﻿using Ninject.Modules;
using Task_24_Advanced.DAL.Interfaces;
using Task_24_Advanced.DAL.Repositories;

namespace Task_24_Advanced.BLL.Infrastructure
{
    /// <summary>
    /// Ninject module
    /// </summary>
    public class ServiceModule : NinjectModule
    {
        private readonly string connectionString;
        /// <summary>
        /// Throw connection string
        /// </summary>
        /// <param name="connection">connection string</param>
        public ServiceModule(string connection)
        {
            connectionString = connection;
        }
        /// <summary>
        /// Bind IUnitOfWork with his realization
        /// </summary>
        public override void Load()
        {
            Bind<IUnitOfWork>().To<EFUnitOfWork>().WithConstructorArgument(connectionString);
        }
    }
}