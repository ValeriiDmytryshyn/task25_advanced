﻿namespace Task_24_Advanced.BLL.DTO
{
    /// <summary>
    /// Base class DTO
    /// </summary>
    public class BaseClassDTO
    {
        /// <summary>
        /// Id of object
        /// </summary>
        public int Id { get; set; }
    }
}