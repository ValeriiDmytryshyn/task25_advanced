﻿using Task_24_Advanced.DAL.Entities;
namespace Task_24_Advanced.BLL.DTO
{
    /// <summary>
    /// DTO tag
    /// </summary>
    public class TagClassDTO : BaseClassDTO
    {
        /// <summary>
        /// tag name
        /// </summary>
        public string Tag { get; set; }
        /// <summary>
        /// empty constructor
        /// </summary>
        public TagClassDTO() { }
        /// <summary>
        /// DTO constructor 
        /// </summary>
        /// <param name="tag">TagClass</param>
        public TagClassDTO(TagClass tag)
        {
            Id = tag.Id;
            Tag = tag.Tag;
        }
        /// <summary>
        /// Constructor that create TagClassDTO
        /// </summary>
        /// <param name="tag"></param>
        public TagClassDTO(string tag){Tag = tag;}
    }
}