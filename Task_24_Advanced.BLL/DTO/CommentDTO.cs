﻿using System;
namespace Task_24_Advanced.BLL.DTO
{
    /// <summary>
    /// Comment model
    /// </summary>
    public class CommentDTO : BaseClassDTO
    {

        /// <summary>
        /// User's name
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// User's comment content
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// Comment date
        /// </summary>

        public DateTime Date { get; set; }
        /// <summary>
        /// Standatr initilize constructor 
        /// </summary>
        /// <param name="id">Comment id</param>
        /// <param name="author">Comment author name</param>
        /// <param name="content">Comment content</param>
        /// <param name="date">Comment date</param>
        public CommentDTO(int id, string author, string content, DateTime date)
        {
            Id = id;
            Author = author;
            Content = content;
            Date = date;
        }
        /// <summary>
        /// Empty constructor
        /// </summary>
        public CommentDTO(){}
    }
}