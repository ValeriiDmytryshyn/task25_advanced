﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.BLL.DTO
{
    /// <summary>
    /// DTO class article
    /// </summary>
    public class ArticleDTO : BaseClassDTO, IArticle
    {
        /// <summary>
        /// Article's title
        /// </summary>
        [Required(ErrorMessage = "Enter article's title")]
        public string Name { get; set; }
        /// <summary>
        /// Article's category
        /// </summary>
        [Required(ErrorMessage = "Enter article's category")]
        public string Category { get; set; }
        /// <summary>
        /// Article's post date
        /// </summary>
        [Required(ErrorMessage = "Enter article's post date")]
        public DateTime Date { get; set; }
        /// <summary>
        /// Article's tags
        /// </summary>
        [Required(ErrorMessage = "Enter article's tags")]
        public List<TagClassDTO> Tags { get; set; } = new List<TagClassDTO>();
        /// <summary>
        /// Article's content
        /// </summary>
        [Required(ErrorMessage = "Enter article's content")]
        public string Content { get; set; }
        /// <summary>
        /// Article's img
        /// </summary>
        [Required(ErrorMessage = "Enter way to image in internet like http://google.com/image")]
        public string WayToImg { get; set; }
        /// <summary>
        /// DTO constructor
        /// </summary>
        /// <param name="article">DTO article</param>
        public ArticleDTO(Article article)
        {
            Id = article.Id;
            Category = article.Category;
            Name = article.Name;
            Date = article.Date;
            Content = article.Content;
            WayToImg = article.WayToImg;
            foreach (var item in article.Tags)
            {
                var tag = item;
                Tags.Add(new TagClassDTO(tag));
            }
        }
        /// <summary>
        /// Initilize constructor
        /// </summary>
        /// <param name="id">Article's id</param>
        /// <param name="category">Article's category</param>
        /// <param name="name">Article's title</param>
        /// <param name="date">Article's date</param>
        /// <param name="content">Article's content</param>
        /// <param name="wayToImg">Article's img</param>
        /// <param name="tags">Article's tags</param>
        public ArticleDTO(int id, string category, string name, DateTime date,
           string content, string wayToImg, List<TagClassDTO> tags)
        {
            Id = id;
            Category = category;
            Name = name;
            Date = date;
            Content = content;
            WayToImg = wayToImg;
            Tags = tags;
        }
        /// <summary>
        /// Empty constructor
        /// </summary>
        public ArticleDTO(){}
    }
}