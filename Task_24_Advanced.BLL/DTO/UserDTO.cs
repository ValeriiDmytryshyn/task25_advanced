﻿namespace Task_24_Advanced.BLL.DTO
{
    /// <summary>
    /// User class
    /// </summary>
    public class UserDTO
    {
        /// <summary>
        /// User's id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// User's email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// User's password
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// User's nickname
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// User's name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// User's address
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// User's role
        /// </summary>
        public string Role { get; set; }
    }
}