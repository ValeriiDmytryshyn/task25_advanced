﻿using System;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.BLL.DTO
{
    /// <summary>
    /// Book model
    /// </summary>
    public class BookDTO : BaseClassDTO, IBook
    {
        /// <summary>
        /// Book title
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Book short description
        /// </summary>
        public string ShortDescription { get; set; }
        /// <summary>
        /// Book author fullname
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Book img
        /// </summary>
        public string LinkToImg { get; set; }
        /// <summary>
        /// Book post date
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Book category
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// Standart initilise model
        /// </summary>
        /// <param name="id">Book id</param>
        /// <param name="category">Book category</param>
        /// <param name="name">Book title</param>
        /// <param name="shortDescription">Book short description</param>
        /// <param name="date">Book post date</param>
        /// <param name="linkToImg">Book img</param>
        /// <param name="author">Book author fullname</param>
        public BookDTO(int id, string category, string name, string shortDescription,
            DateTime date, string linkToImg, string author)
        {
            Id = id;
            Category = category;
            Name = name;
            ShortDescription = shortDescription;
            Date = date;
            LinkToImg = linkToImg;
            Author = author;
        }
        /// <summary>
        /// DTO constructor
        /// </summary>
        /// <param name="book">DTO book</param>
        public BookDTO(Book book)
        {
            Id = book.Id;
            Category = book.Category;
            Name = book.Name;
            ShortDescription = book.ShortDescription;
            Date = book.Date;
            LinkToImg = book.LinkToImg;
            Author = book.Author;
        }
        /// <summary>
        /// Empty constructor
        /// </summary>
        public BookDTO() { }
    }
}