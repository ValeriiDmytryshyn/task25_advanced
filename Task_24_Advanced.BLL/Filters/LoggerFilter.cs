﻿using System;
using System.Web;
using System.Web.Mvc;
namespace Task_24_Advanced.Filters
{
    /// <summary>
    /// Logger filter
    /// </summary>
    public class LoggerFilterAttribute : Attribute, IActionFilter
    {
        /// <summary>
        /// Called before calling the method
        /// </summary>
        /// <param name="filterContext">Action executed context</param>
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Cookies.Add(new HttpCookie(filterContext.ActionDescriptor.ActionName, DateTime.Now.ToString()));
        }
        /// <summary>
        /// Called after calling the method
        /// </summary>
        /// <param name="filterContext">Action executed context</param>
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            throw new NotImplementedException();
        }
    }
}