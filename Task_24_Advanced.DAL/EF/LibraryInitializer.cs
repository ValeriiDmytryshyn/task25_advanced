﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Entities.Identity;
using Task_24_Advanced.DAL.Identity;

namespace Task_24_Advanced.DAL.EF
{
    /// <summary>
    /// Initialize standart data into db
    /// </summary>
    public class LibraryInitializer : DropCreateDatabaseAlways<LibraryContext>
    {
        /// <summary>
        /// Override EF context method
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(LibraryContext context)
        {
            List<Book> Books = new List<Book>
                {
                    new Book(0,"Top Six" , "Blackout: How Black America Can Make Its Second Escape from the Democrat Plantation",
                        "Political activist and social media star Candace Owens addresses the many ways that Democrat" +
                        " Party policies hurt, rather than help, the African American community, and why she and many" +
                        " others are turning right..",
                        DateTime.Now, "https://m.media-amazon.com/images/I/71kA1NW-cAL._AC_UY218_.jpg",
                        "Candace Owens"),
                    new Book(1,"Top Six", "Disloyal: A Memoir: The True Story of the Former Personal Attorney to President Donald J. Trump",
                        "'I read it cover-to-cover. I did not intend to, but I started at the beginning and didn’t" +
                        " put it down until it was over.'—Rachel Maddow, MSNBC",
                        DateTime.Now, "https://m.media-amazon.com/images/I/71GslFSfZNL._AC_UY218_.jpg",
                        "Michael Cohen"),
                    new Book(2,"Top Six", "Dog Man: Grime and Punishment: From the Creator of Captain Underpants (Dog Man #9)",
                        "The mayor has had enough of Dog Man's shenanigans in the ninth book from " +
                        "worldwide bestselling author and artist Dav Pilkey.",
                        DateTime.Now, "https://m.media-amazon.com/images/I/81-PR6tVo2L._AC_UY218_.jpg",
                        "Dav Pilkey"),
                    new Book(3,"Top Six", "Midnight Sun",
                        "It's here! #1 bestselling author Stephenie Meyer makes a triumphant return" +
                        " to the world of Twilight with this highly anticipated companion: the iconic" +
                        " love story of Bella and Edward told from the vampire's point of view.",
                        DateTime.Now, "https://m.media-amazon.com/images/I/81zBVMvSjNL._AC_UY218_.jpg",
                        "Stephenie Meyer"),
                    new Book(4,"Top Six", "The Home Edit Life: The No-Guilt Guide to Owning What You Want and Organizing Everything",
                        "#1 NEW YORK TIMES BESTSELLER • The New York Times bestselling authors of" +
                        " The Home Edit and stars of the Netflix series Get Organized with The Home" +
                        " Edit teach you how to apply their genius, holistic approach to your work" +
                        " life, on-the-go necessities, and technology.",
                        DateTime.Now, "https://m.media-amazon.com/images/I/811ZWr6i51L._AC_UY218_.jpg",
                        "Clea Shearer"),
                    new Book(5, "Top Six","Caste (Oprah's Book Club): The Origins of Our Discontents",
                        "NEW YORK TIMES BESTSELLER • OPRAH’S BOOK CLUB PICK • The Pulitzer " +
                        "Prize–winning, bestselling author of The Warmth of Other Suns examines" +
                        " the unspoken caste system that has shaped America and shows how our lives" +
                        " today are still defined by a hierarchy of human divisions..",
                        DateTime.Now, "https://m.media-amazon.com/images/I/811opppMPQL._AC_UY218_.jpg",
                        "Isabel Wilkerson"),
                    new Book(6,"Hot Books", "Hideaway: A Novel",
                    "'Reading Hideaway is like a mini vacation, as Roberts transports you" +
                    " from the sun-drenched mountains of Big Sur to the rolling hills of " +
                    "Ireland to the bustling streets of New York City.' - Associated Press",
                    DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/51qS5vTXIcL._SX327_BO1,204,203,200_.jpg",
                    "Nora Roberts"),
                    new Book(7, "Hot Books","The Happy in a Hurry Cookbook: 100-Plus " +
                    "Fast and Easy New Recipes That Taste Like Home (The Happy Cookbook Series)",
                    "In this follow up to their New York Times bestseller The Happy Cookbook," +
                    " Fox & Friends cohost Steve Doocy and his wife, Kathy, share more hilarious" +
                    " stories and offer crowd-pleasing recipes that are quick, easy, and delicious.",
                    DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/51KRcxm2WXL._SX400_BO1,204,203,200_.jpg",
                    "Steve Doocy"),
                    new Book(8, "Hot Books","The Office: A Day at Dunder Mifflin Elementary",
                    "Discover 'The Office reboot fans never knew they needed' with this kid-friendly" +
                    " adaptation of everyone's favorite workplace comedy (Entertainment Weekly).",
                    DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/51tlHFn8rwL._SX389_BO1,204,203,200_.jpg",
                    "Robb Pearlman"),
                    new Book(9, "Hot Books","Modern Comfort Food: A Barefoot Contessa Cookbook",
                    "A collection of all-new soul-satisfying dishes from America’s favorite home cook!",
                    DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/51N2z-uibpL._SX373_BO1,204,203,200_.jpg",
                    "Ina Garten"),
                    new Book(10, "Hot Books","Rage",
                    "Bob Woodward’s new book, Rage, is an unprecedented and intimate tour de force" +
                    " of new reporting on the Trump presidency facing a global pandemic, economic" +
                    " disaster and racial unrest.",
                    DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/41r4MsfTSxL._SX329_BO1,204,203,200_.jpg",
                    "Bob Woodward"),
                    new Book(11, "Hot Books","Humans",
                    "Brandon Stanton’s new book, Humans―his most moving and compelling book to date―shows us the world.",
                    DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/51B-BOBtQtL._SX394_BO1,204,203,200_.jpg",
                    "Brandon Stanton"),
                    new Book(12, "Hot Books","One Vote Away: How a Single Supreme Court Seat Can Change History",
                    "With Justice Ruth Bader Ginsburg’s sudden passing, control of the Supreme Court—and with it " +
                    "the fate of the Constitution—has become the deciding issue for many voters in the 2020" +
                    " presidential election. And the stakes could not be higher.",
                    DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/41-R079jwKL._SX329_BO1,204,203,200_.jpg",
                    "Ted Cruz"),
                    new Book(13, "Hot Books", "The Meaning of Mariah Carey",
                    "The global icon, award-winning singer, songwriter, producer, actress, mother, daughter," +
                    " sister, storyteller, and artist finally tells the unfiltered story of her life in The Meaning " +
                    "of Mariah Carey",
                    DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/51Cmn6qrXcL._SX327_BO1,204,203,200_.jpg",
                    "Mariah Carey"),
                    new Book(14, "Most Wished Books", "Goodnight Moon",
                        "In a great green room, tucked away in bed, is a little bunny. " +
                        "'Goodnight room, goodnight moon.' And to all the familiar things in " +
                        "the softly lit room—to the picture of the three little bears sitting " +
                        "on chairs, to the clocks and his socks, to the mittens and the kittens," +
                        " to everything one by one—the little bunny says goodnight.",
                        DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/51+mV1XUUQL._AC_SX184_.jpg",
                        "Clement Hurd"),
                    new Book(15, "Most Wished Books","Brown Bear, Brown Bear, What Do You See?",
                        "Handpicked by Amazon kids’ books editor, Seira Wilson, for Prime Book Box" +
                        " – a children’s subscription that inspires a love of reading.",
                        DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/51430n+9jlL._AC_SX184_.jpg",
                        "Bill Martin Jr."),
                    new Book(16, "Most Wished Books","Dr. Seuss's Beginner Book Collection (Cat in the Hat, One Fish" +
                        " Two Fish, Green Eggs and Ham, Hop on Pop, Fox in Socks)",
                        "Celebrate back-to-school—no matter what that looks like—with this " +
                        "collectible Dr. Seuss boxed set that’s perfect for inspiring a love of reading!",
                        DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/51PWDGLykIL._AC_SX184_.jpg",
                        "Dr. Seuss"),
                    new Book(17, "Most Wished Books","The Home Edit: A Guide to Organizing and Realizing Your" +
                        " House Goals (Includes Refrigerator Labels)",
                        "NEW YORK TIMES BESTSELLER • From the stars of the Netflix series Get " +
                        "Organized with The Home Edit (with a serious fan club that includes Reese" +
                        " Witherspoon, Gwyneth Paltrow, and Mindy Kaling), here is an accessible, " +
                        "room-by-room guide to establishing new order in your home.",
                        DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/51PAyDylglL._AC_SX184_.jpg",
                        "Clea Shearer"),
                    new Book(18, "Most Wished Books","Where the Wild Things Are",
                        "Maurice Sendak's Caldecott Medal-winning picture book has become one of" +
                        " the most highly acclaimed and best-loved children's books of all time." +
                        " A must for every child's bookshelf.",
                        DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/61ulN35aErL._AC_SX184_.jpg",
                        "Maurice Sendak"),
                    new Book(19, "Most Wished Books","Indestructibles: Twinkle, Twinkle, Little Star: Chew Proof · " +
                        "Rip Proof · Nontoxic · 100% Washable (Book for Babies, Newborn Books, Safe to Chew)",
                        "Sing a nursery rhyme with a book that’s Indestructible!",
                        DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/51nTNxEPMLL._AC_SX184_.jpg",
                        "Maddie Frost"),
                    new Book(20, "Most Wished Books","The Giving Tree",
                        "The Giving Tree, a story of unforgettable perception, beautifully written " +
                        "and illustrated by the gifted and versatile Shel Silverstein, has been a" +
                        " classic favorite for generations.",
                        DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/41fR2KV7jzL._AC_SX184_.jpg",
                        "Shel Silverstein"),
                    new Book(21, "Most Wished Books","Love You Forever",
                        "A young woman holds her newborn son And looks at him lovingly.",
                        DateTime.Now, "https://images-na.ssl-images-amazon.com/images/I/61k5YUaOrZL._AC_SX184_.jpg",
                        "Robert Munsch"),
                new Book(22, "IT Books", "Cracking the Coding Interview: 189 Programming Questions and Solutions",
                    "I am not a recruiter. I am a software engineer. And as such, I know what it's" +
                    " like to be asked to whip up brilliant algorithms on the spot and then write " +
                    "flawless code on a whiteboard. I've been through this as a candidate and as an interviewer.",
                    DateTime.Now, "https://m.media-amazon.com/images/I/61mIq2iJUXL._AC_UY218_.jpg",
                    "Gayle Laakmann McDowell"),
                new Book(23, "IT Books","HTML and CSS: Design and Build Websites",
                    "A full-color introduction to the basics of HTML and CSS from the publishers of Wrox! " +
                    "Every day, more and more people want to learn some HTML and CSS. Joining the " +
                    "professional web designers and programmers are new audiences who need to know" +
                    " a little bit of code at work (update a content management system or e-commerce" +
                    " store) and those who want to make their personal blogs more attractive.",
                    DateTime.Now, "https://m.media-amazon.com/images/I/31aX81I6vnL._AC_UY218_.jpg",
                    "Jon Duckett"),
                new Book(24, "IT Books","Eloquent JavaScript, 3rd Edition: A Modern Introduction to Programming",
                    "Completely revised and updated, this best-selling introduction to programming " +
                    "in JavaScript focuses on writing real applications.",
                    DateTime.Now, "https://m.media-amazon.com/images/I/91q8Jx+j6iL._AC_UY218_.jpg",
                    "Marijn Haverbeke"),
                new Book(25, "IT Books","System Design Interview – An insider's guide, Second Edition",
                    "The system design interview is considered to be the most complex and " +
                    "most difficult technical job interview by many. Those questions are" +
                    " intimidating, but don’t worry. It's just that nobody has taken the" +
                    " time to prepare you systematically.",
                    DateTime.Now, "https://m.media-amazon.com/images/I/61HlS-BupKL._AC_UY218_.jpg",
                    "Alex Xu"),
                new Book(26, "IT Books","The Pragmatic Programmer: 20th Anniversary Edition, 2nd Edition: Your Journey to Mastery",
                    "The Pragmatic Programmer is one of those rare tech audiobooks" +
                    " you’ll listen, re-listen, and listen to again over the years. Whether" +
                    " you’re new to the field or an experienced practitioner, you’ll come " +
                    "away with fresh insights each and every time. ",
                    DateTime.Now, "https://m.media-amazon.com/images/I/91WFb-PpoNL._AC_UY218_.jpg",
                    "David Thomas"),
                new Book(27, "IT Books","Clean Code: A Handbook of Agile Software Craftsmanship",
                    "Even bad code can function. But if code isn’t clean, it can bring a" +
                    " development organization to its knees. Every year, countless hours" +
                    " and significant resources are lost because of poorly written code." +
                    " But it doesn’t have to be that way.",
                    DateTime.Now, "https://m.media-amazon.com/images/I/41-+g1a2Y1L._AC_UY218_.jpg",
                    "Robert C. Martin"),
                new Book(28, "IT Books","Automate the Boring Stuff with Python, 2nd Edition: Practical Programming for Total Beginners",
                    "The second edition of this best-selling Python book (100,000+ copies" +
                    " sold in print alone) uses Python 3 to teach even the technically " +
                    "uninclined how to write programs that do in minutes what would take" +
                    " hours to do by hand. There is no prior programming experience required" +
                    " and the book is loved by liberal arts majors and geeks alike.",
                    DateTime.Now, "https://m.media-amazon.com/images/I/8187Fs2zmrL._AC_UY218_.jpg",
                    "Al Sweigart"),
                new Book(29, "IT Books","Introduction to Algorithms, 3rd Edition (The MIT Press)",
                    "The latest edition of the essential text and professional reference, " +
                    "with substantial new material on such topics as vEB trees, multithreaded" +
                    " algorithms, dynamic programming, and edge-based flow.",
                    DateTime.Now, "https://m.media-amazon.com/images/I/61uRpcdPhNL._AC_UY218_.jpg",
                    "Thomas H. Cormen")
            };
            Books.ForEach(s => context.Books.Add(s));
            context.SaveChanges();

            List<Comment> Comments = new List<Comment>
            {
                new Comment(0, "Valerii" ,
                "Nice site!",
                DateTime.Now),
                new Comment(1, "Anderey" ,
                "Very usefull site.",
                DateTime.Now),
                new Comment(2, "Max" ,
                "The best impressions of ever.",
                DateTime.Now),
                new Comment(3, "Eduard" ,
                "Could be better.",
                DateTime.Now),
                new Comment(4, "Oleg" ,
                "Content nice, but want another design.",
                DateTime.Now),
                new Comment(5, "Vlad" ,
                "I like it",
                DateTime.Now),
                new Comment(6, "Anton" ,
                "I'm mad about it!",
                DateTime.Now),
                new Comment(7, "Bogdan" ,
                "I'm crazy about it.",
                DateTime.Now)
            };
            Comments.ForEach(s => context.Comments.Add(s));
            context.SaveChanges();

            List<Article> Articles = new List<Article>{
                new Article(0, "Mini article", "The Age of Innocence is a gang story as brutal as Goodfellas", DateTime.Now,
                "Even in 1993, it seemed surprising that Martin Scorsese should direct an adaptation of The Age of Innocence." +
                " Why was the director of bloody and furious classics such as Taxi Driver and Raging Bull taking on this story" +
                " of decorum and reserve in New York high society? When the film came out, critic Roger Ebert wrote that the" +
                " pairing had “struck many people as astonishing – as surprising, say, as if Abel Ferrara had announced" +
                " a film by Henry James”. ",
                "https://i.guim.co.uk/img/media/b4f9bdceea06c86f428e58a28474ce0971c83dc1/0_64_3184_1910/master/3184.jpg?width=620&quality=85&auto=format&fit=max&s=fc44b61bd99a618e992f1da2e8b19e74"
                , new List<TagClass>(){ new TagClass("History"), new TagClass("Mini article")}),
                new Article(1, "Mini article","Tips, links and suggestions: what are you reading this week?", DateTime.Now,
                "I am loving seeing a resurgence of Rebecca on my Instagram feed. I know there is always a great" +
                " deal of love for this du Maurier and the @netflixuk upcoming adaption is doing nothing " +
                "to dampen that. Do you have a favourite du Maurier?",
                "https://english.colostate.edu/wp-content/uploads/sites/56/2018/06/thesenovelthoughts-1-585x600.jpg"
                , new List<TagClass>(){ new TagClass("Tips"), new TagClass("Mini article")}),
                new Article(2, "Mini article","JK Rowling's Troubled Blood: don't judge a book by a single review", DateTime.Now,
                "Before it had even come out, criticism of JK Rowling’s new Robert Galbraith thriller, Troubled " +
                "Blood, was already wall-to-wall, after an early review in the Telegraph claimed that its “moral " +
                "seems to be: never trust a man in a dress”.But is that the moral of the book? I’ve read it," +
                " the latest outing for Rowling’s private detective Cormoran Strike and his partner Robin" +
                " Ellacott.In Troubled Blood, they have been tasked with investigating the disappearance of" +
                " GP Margot Bamborough more than 40 years earlier. As the pair look into the disappearance – " +
                "and this is a spoiler – one of the avenues they investigate is the possibility Margot was",
                "https://i.guim.co.uk/img/media/8ba8edb24632f56a74659b5e6dc2733856d4cacd/0_232_3500_2101/master/3500.jpg?width=620&quality=85&auto=format&fit=max&s=22d721a7e58a4a2623bb1dfd3766fe63"
                , new List<TagClass>(){ new TagClass("History"), new TagClass("Mini article")}),
                 new Article(3, "Mini article","Judging the Booker prize: 'These books are about living under intense pressure'", DateTime.Now,
                "For the Booker judges, like everyone else, 2020 has been a year of staying in. Luckily for us, we had the" +
                " company of 162 authors to keep us entertained. Months of reading, rereading and debating over Zoom has " +
                "enabled us to whittle down the set to six books. Diane Cook’s The New Wilderness follows a group " +
                "of people trying to survive in a near-future blighted by environmental catastrophe. The subject matter" +
                " resonated for us in an era of climate disasters, but it was the touching relationship between the mother" +
                " and daughter that kept us reading.Another mother - daughter drama, this time set in India, is precisely ",
                "https://i.guim.co.uk/img/media/518cb0d1f59a59d786298207ff29d87a3faa8cf0/518_363_2956_1774/master/2956.jpg?width=620&quality=85&auto=format&fit=max&s=bf5591c9c39bba89fd3dd01afee77616"
                 , new List<TagClass>(){ new TagClass("Judging the Booker prize"), new TagClass("Mini article")}),
                 new Article(4, "Mini article","Reading group: The Age of Innocence by Edith Wharton is our book for September", DateTime.Now,
                "For this month’s reading group, we’re going to look at Edith Wharton’s 1920 masterpiece, The Age of " +
                "Innocence. It’s 100 years since the publication of this story of New York society, unhappy couples, " +
                "loss and love – and it’s going to be fascinating to see how it measures up today. Not least because " +
                "the passing of time is one of its chief sources of inspiration. Wharton set the novel in the Gilded " +
                "Age, in 1870s New York society, and said that in writing the book, she found “a momentary escape in " +
                "going back to my childish memories of a long-vanished America … it was growing more and more evident " +
                "that the world I had grown up in and been formed by had been destroyed in 1914.”",
                "https://i.guim.co.uk/img/media/ea9627b8edd697a3196c64b42231adac04479e1a/422_670_2648_1588/master/2648.jpg?width=620&quality=85&auto=format&fit=max&s=90f04f9eec3d28886d421016f56ed51d"
                , new List<TagClass>(){ new TagClass("Reading group"), new TagClass("Mini article")}),
                 new Article(5, "Mini article","Poem of the week: Felix Randal by Gerard Manley Hopkins", DateTime.Now.AddDays(-7),
                "Felix RandalFelix Randal the farrier, O is he dead then? my duty all ended,Who have watched his mould" +
                " of man, big-boned and hardy-handsome Pining, pining, till time when reason rambled in it, and some" +
                "Fatal four disorders, fleshed there, all contended?",
                "https://i.guim.co.uk/img/media/683bb3f9c4689f4903db7f78a136c8beef0d6be2/0_341_2598_1557/master/2598.jpg?width=620&quality=85&auto=format&fit=max&s=fe1c0cb1674ac67d209ca486a4e6ca18"
                , new List<TagClass>(){ new TagClass("Poem of the week"), new TagClass("Mini article")}),
                 new Article(6, "Mini article","Reading group: which PD James novel should we read this month?", DateTime.Now.AddDays(7),
                "This month on the reading group, we’re going to read a book by PD James. This week marks 100 years" +
                " since the birth of the writer known not only as “the new queen of crime”, and one of the last" +
                " links to the genre’s golden age (she was dubbed the new queen after Agatha Christie’s death," +
                " a title James did not mind), but also “the doyenne of detective novelists”.",
                "https://i.guim.co.uk/img/media/2b1350eb747393de3916f3d6d03383656a0d0228/0_12_4256_2558/master/4256.jpg?width=620&quality=85&auto=format&fit=max&s=ec3320c4739766cc3e306ce2e972ece5"
                , new List<TagClass>(){ new TagClass("Reading group"), new TagClass("Mini article")}),
                new Article(7, "Mini article","First George RR Martin, now Patrick Rothfuss: the curse of sequel-hungry fans", DateTime.Now,
                "In Misery, Stephen King created Annie Wilkes, a fanatical reader who becomes so upset at the ending " +
                "that befalls her favourite character Misery Chastain, that she captures the author, Paul Sheldon – then" +
                " cuts off various parts of his body while forcing him to write the story she wants.",
                "https://i.guim.co.uk/img/media/9d00ea5a5769d596a176632dfd31e1921c109228/0_192_4746_2850/master/4746.jpg?width=620&quality=85&auto=format&fit=max&s=aad289fe8fb39650cc34bf47debadfb3"
                , new List<TagClass>(){ new TagClass("Reading group"), new TagClass("Mini article")}),
                new Article(8, "Mini article","Publishing has ignored and pigeonholed black authors for too long", DateTime.Now,
                "Over the past two weeks, people around the globe have gathered in protest against the terrible deaths" +
                " of George Floyd, Breonna Taylor and Ahmaud Arbery. In the UK, within just a few days, it was revealed" +
                " that the case of Belly Mujinga, who died of coronavirus after being spat at in Victoria station, had" +
                " been closed while the government’s report on race and Covid-19 showed that, as suspected, black people" +
                " are disproportionately dying from the virus. ",
                "https://i.guim.co.uk/img/media/f8267d4aa639b409d0e3fc6ccad4e6c2762f928f/365_1141_7842_4706/master/7842.jpg?width=620&quality=85&auto=format&fit=max&s=119dc64a765f8d9f7610e59ab856c1ba"
                , new List<TagClass>(){ new TagClass("Racism"), new TagClass("Mini article")}),
                new Article(9, "Mini article","How White Teeth transcends its many flaws", DateTime.Now,
                "“I’d written what was meant to be a short story – which was kind of the first two chapters. I got a letter" +
                " from this publisher who’d read a short story of mine in an Oxbridge collection of short fiction … He said" +
                " have you got anything longer and I sent him what I had of this long story and that was that … And the scary" +
                " thing was then being told I had to finish it and write this novel.”",
                "https://i.guim.co.uk/img/media/59734b015fceb309e9c5fa24051834aafd434851/0_67_2000_1200/master/2000.jpg?width=620&quality=85&auto=format&fit=max&s=1c67f4bdf69e1ecb86130f4b8ee552bf"
                , new List<TagClass>(){ new TagClass("Theatr"), new TagClass("Mini article")}),
                new Article(10, "Main article","White Teeth seemed fresh and hopeful in 2000 – how does it read now?", DateTime.Now,
                "Zadie Smith’s White Teeth, the back of my copy explains, is about “the tricky way the past has of " +
                "coming back and biting you on the ankle”. A statement made all the more interesting in the 20 years" +
                " since it came out; a book obsessed with the past, which has itself become a thing of the past.",
                "/Content/img/MainArticleFirst.jpg", new List<TagClass>()
                { new TagClass("How does it read"), new TagClass("Main article")}
                ),
                new Article(11, "Main article","A bookshop reopens: 'Customers are awkwardly dancing around each other'", DateTime.Now,
                "Our first day back since mid-March started at 5am with those same niggling doubts you get before" +
                " going on holiday: what vital thing have I forgotten? By 6.30am I’m in the bookshop and waiting for" +
                " a BBC crew to arrive – today we will be juggling a whole new set of rules and a film crew … We’ve " +
                "done all the things required: distance markers, piles of disposable gloves, directions printed in " +
                "the largest font possible, and enough sanitising gel to cleanse all the souls in purgatory. But customers" +
                " have to take responsibility too. No shopkeeper wants to be constantly barking commands: “Use the hand gel!”," +
                " “Please do try not to cuddle all the books.”",
                "/Content/img/MainArticleSecond.jpg", new List<TagClass>()
                { new TagClass("Bookshop"), new TagClass("Main article")}
                ),
                new Article(12,"Main article", "Do the work: an anti-racist reading list", DateTime.Now,
                "Every black person I know right now is exhausted. They are exhausted by the two pandemics" +
                " disproportionately hurting and killing black people: Covid-19 and white supremacy." +
                " Covid-19 is a new sickness that hopefully we’ll soon find a cure for, or at least " +
                "learn to live with. But white supremacy is a disease as old as time, for which we’ve " +
                "As we mourn and seek justice for the murders of George Floyd, Ahmaud Arbery, Breonna Taylor," +
                " Nina Pop and Tony McDade (to name but a few), many black people such as myself are wondering:" +
                " what will happen when the news cycle is over, the social justice memes are no longer posted, and " +
                "the declarations for inclusivity, diversity and “doing the work” have died down?" +
                " been waiting generations to see a cure.", "/Content/img/MainArticleThird.jpg",
                new List<TagClass>()
                { new TagClass("Covid"), new TagClass("Main article")})
            };
            Articles.ForEach(s => context.Articles.Add(s));
            context.SaveChanges();
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // создаем две роли
            var role1 = new IdentityRole { Name = "admin" };
            var role2 = new IdentityRole { Name = "user" };

            // добавляем роли в бд
            roleManager.Create(role1);
            roleManager.Create(role2);

            // создаем пользователей
            var admin = new ApplicationUser { Email = "admin@gmail.admin", UserName = "admin@gmail.admin" };
            string password = "P@ssw0rd";
            var user = new ApplicationUser { Email = "user@gmail.admin", UserName = "user@gmail.admin" };
            string userPassword = "P@ssw0rd";
            var result = userManager.Create(admin, password);
            var userResult = userManager.Create(user, userPassword);

            // если создание пользователя прошло успешно
            if (result.Succeeded && userResult.Succeeded)
            {
                // добавляем для пользователя роль
                userManager.AddToRole(admin.Id, role1.Name);
                userManager.AddToRole(user.Id, role2.Name);
                userManager.AddToRole(admin.Id, role2.Name);
            }

            base.Seed(context);
            context.SaveChanges();
        }
    }
}