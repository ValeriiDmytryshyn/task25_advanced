﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Entity;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Entities.Identity;

namespace Task_24_Advanced.DAL.EF
{
    /// <summary>
    /// Connect this project with db
    /// </summary>
    public class LibraryContext : IdentityDbContext<ApplicationUser>
    {
        //public LibraryContext(string connectionString) : base("DefaultConnection")
        //{
        //    Database.SetInitializer<LibraryContext>(new LibraryInitializer());
        //}
        public LibraryContext(string connectionString) : base(connectionString)
        {
            Database.SetInitializer<LibraryContext>(new LibraryInitializer());
        }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Form> Forms { get; set; }
        public DbSet<ClientProfile> ClientProfiles { get; set; }
    }
}