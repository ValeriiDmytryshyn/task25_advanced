﻿using System.Collections.Generic;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.DAL.Entities
{
    public class Form : BaseClass, IForm
    {
        public string Author { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }

        public string Gender { get; set; }

        /// <summary>
        /// User can leave this field blank
        /// </summary>
        public List<string> WhichDevice { get; set; }
        /// <summary>
        /// User can leave this field blank
        /// </summary>
        public List<string> WhichContent { get; set; }

        public string Content { get; set; }
        public Form()
        {

        }
        public Form(int id, string author, string email, int age,
                    string gender, List<string> whichDevice,
                    List<string> whichContent, string content)
        {
            Id = id;
            Author = author;
            Email = email;
            Age = age;
            Gender = gender;
            WhichDevice = whichDevice;
            WhichContent = whichContent;
            Content = content;
        }
    }
}