﻿using System;
using System.Collections.Generic;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.DAL.Entities
{
    public class Article : BaseClass, IArticle
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public List<TagClass> Tags { get; set; } = null;
        public DateTime Date { get; set; }
        public string Content { get; set; }
        public string WayToImg { get; set; }
        public Article(int id, string category, string name, DateTime date, string content, string wayToImg)
        {
            Id = id;
            Category = category;
            Name = name;
            Date = date;
            Content = content;
            WayToImg = wayToImg;
        }
        public Article(int id, string category, string name, DateTime date,
            string content, string wayToImg, List<TagClass> tags)
        {
            Id = id;
            Category = category;
            Name = name;
            Date = date;
            Content = content;
            WayToImg = wayToImg;
            Tags = tags;
        }
        public Article()
        {

        }
    }
}