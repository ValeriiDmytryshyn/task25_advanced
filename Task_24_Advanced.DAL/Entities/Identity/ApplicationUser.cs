﻿using Microsoft.AspNet.Identity.EntityFramework;
namespace Task_24_Advanced.DAL.Entities.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public virtual ClientProfile ClientProfile { get; set; }
    }
}