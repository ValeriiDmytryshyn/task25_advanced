﻿namespace Task_24_Advanced.DAL.Entities
{
    public class TagClass : BaseClass
    {
        public string Tag { get; set; }
        public TagClass(int id, string tag)
        {
            Id = id;
            Tag = tag;
        }
        public TagClass(string tag)
        {
            Tag = tag;
        }
        public TagClass()
        {

        }
    }
}