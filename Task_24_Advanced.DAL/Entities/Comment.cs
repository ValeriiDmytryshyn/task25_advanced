﻿using System;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.DAL.Entities
{
    public class Comment : BaseClass, IComment
    {
        
        public string Author { get; set; }
        public string Content { get; set; }

        public DateTime Date { get; set; }
        public Comment(int id, string author, string content, DateTime date)
        {
            Id = id;
            Author = author;
            Content = content;
            Date = date;
        }
        public Comment()
        {

        }

    }
}