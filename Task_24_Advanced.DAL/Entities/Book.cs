﻿using System;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.DAL.Entities
{
    public class Book : BaseClass, IBook
    {
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string Author { get; set; }
        public string LinkToImg { get; set; }
        public DateTime Date { get; set; }
        public string Category { get; set; }
        public Book(int id, string category, string name, string shortDescription,
            DateTime date, string linkToImg, string author)
        {
            Id = id;
            Category = category;
            Name = name;
            ShortDescription = shortDescription;
            Date = date;
            LinkToImg = linkToImg;
            Author = author;
        }
        public Book()
        {

        }
    }
}