﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Task_24_Advanced.DAL.EF;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.DAL.Repositories
{
    public class FormRepository : IRepository<Form>
    {
        private readonly LibraryContext db;
        public FormRepository(LibraryContext context)
        {
            this.db = context;
        }
        public int Count()
        {
            return db.Forms.Count();
        }
        public void Create(Form item)
        {
            db.Forms.Add(item);
        }

        public void Delate(int id)
        {
            Form Form = db.Forms.Find(id);
            if (Form != null)
            {
                db.Forms.Remove(Form);
            }
        }

        public Form GetItem(int id)
        {
            return db.Forms.Find(id);
        }

        public IEnumerable<Form> ListItems()
        {
            return db.Forms;
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public void Update(Form item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}