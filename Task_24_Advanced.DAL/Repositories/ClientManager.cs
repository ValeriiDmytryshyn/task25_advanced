﻿using Task_24_Advanced.DAL.EF;
using Task_24_Advanced.DAL.Entities.Identity;
using Task_24_Advanced.DAL.Interfaces.Identity;
namespace Task_24_Advanced.DAL.Repositories
{
    internal class ClientManager : IClientManager
    {
        private readonly LibraryContext db;

        public ClientManager(LibraryContext db)
        {
            this.db = db;
        }
        public void Create(ClientProfile item)
        {
            db.ClientProfiles.Add(item);
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}