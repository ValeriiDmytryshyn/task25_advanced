﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using Task_24_Advanced.DAL.EF;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Entities.Identity;
using Task_24_Advanced.DAL.Identity;
using Task_24_Advanced.DAL.Interfaces;
using Task_24_Advanced.DAL.Interfaces.Identity;

namespace Task_24_Advanced.DAL.Repositories
{
    /// <summary>
    /// Unit of work
    /// </summary>
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly LibraryContext db;
        private ArticleRepository articleRepository;
        private BookRepository bookRepository;
        private CommentRepository commentRepository;
        private FormRepository formRepository;
        private readonly ApplicationUserManager userManager;
        private readonly ApplicationRoleManager roleManager;
        private readonly IClientManager clientManager;
        /// <summary>
        /// Throw db connection string 
        /// </summary>
        /// <param name="connectionString">Db connection string</param>
        public EFUnitOfWork(string connectionString)
        {
            db = new LibraryContext(connectionString);
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(db));
            clientManager = new ClientManager(db);
        }
        /// <summary>
        /// Articles in db
        /// </summary>
        public IRepository<Article> Articles
        {
            get
            {
                if (articleRepository == null)
                    articleRepository = new ArticleRepository(db);
                return articleRepository;
            }
        }
        /// <summary>
        /// Books in db
        /// </summary>
        public IRepository<Book> Books
        {
            get
            {
                if (bookRepository == null)
                    bookRepository = new BookRepository(db);
                return bookRepository;
            }
        }
        /// <summary>
        /// Comments in db
        /// </summary>
        public IRepository<Comment> Comments
        {
            get
            {
                if (commentRepository == null)
                    commentRepository = new CommentRepository(db);
                return commentRepository;
            }
        }
        /// <summary>
        /// Forms in db
        /// </summary>
        public IRepository<Form> Forms
        {
            get
            {
                if (formRepository == null)
                    formRepository = new FormRepository(db);
                return formRepository;
            }
        }
        /// <summary>
        /// User Manager
        /// </summary>
        public ApplicationUserManager UserManager
        {
            get { return userManager; }
        }
        /// <summary>
        /// Client Manager
        /// </summary>
        public IClientManager ClientManager
        {
            get { return clientManager; }
        }
        /// <summary>
        /// Role Manager
        /// </summary>
        public ApplicationRoleManager RoleManager
        {
            get { return roleManager; }
        }
        /// <summary>
        /// Save changes in db
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }
        private bool disposed = false;

        /// <summary>
        /// Dispose data 
        /// </summary>
        /// <param name="disposing">Disposedata(true/false)</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }
        /// <summary>
        /// Dispose data
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}