﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Task_24_Advanced.DAL.EF;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.DAL.Repositories
{
    public class ArticleRepository : IRepository<Article>
    {
        private readonly LibraryContext db;
        public ArticleRepository(LibraryContext context)
        {
            this.db = context;
        }
        public int Count()
        {
            return db.Articles.Count();
        }
        public void Create(Article item)
        {
            db.Articles.Add(item);
        }

        public void Delate(int id)
        {
            Article Article = db.Articles.Include(x => x.Tags).First(x => x.Id == id);
            if (Article != null)
            {
                db.Articles.Remove(Article);
            }
        }

        public Article GetItem(int id)
        {
            var article = db.Articles.Include(x => x.Tags).ToList().Find(x => x.Id == id);
            return article;
        }

        public IEnumerable<Article> ListItems()
        {
            return db.Articles.Include(x => x.Tags);
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public void Update(Article item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}