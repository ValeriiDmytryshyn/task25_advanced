﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Task_24_Advanced.DAL.EF;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.DAL.Repositories
{

    public class CommentRepository : IRepository<Comment>
    {
        private readonly LibraryContext db;
        public CommentRepository(LibraryContext context)
        {
            this.db = context;
        }
        public int Count()
        {
            return db.Comments.Count();
        }
        public void Create(Comment item)
        {
            db.Comments.Add(item);
        }

        public void Delate(int id)
        {
            Comment Comment = db.Comments.Find(id);
            if (Comment != null)
            {
                db.Comments.Remove(Comment);
            }
        }

        public Comment GetItem(int id)
        {
            return db.Comments.Find(id);
        }

        public IEnumerable<Comment> ListItems()
        {
            return db.Comments;
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public void Update(Comment item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}