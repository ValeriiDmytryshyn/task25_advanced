﻿using Task_24_Advanced.DAL.EF;
using Task_24_Advanced.DAL.Entities.Identity;
using Task_24_Advanced.DAL.Interfaces.Identity;

namespace Task_24_Advanced.DAL.Repositories
{
    public class ClientManagerRepository : IClientManager
    {
        public LibraryContext Database { get; set; }
        public ClientManagerRepository(LibraryContext db)
        {
            Database = db;
        }

        public void Create(ClientProfile item)
        {
            Database.ClientProfiles.Add(item);
            Database.SaveChanges();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}