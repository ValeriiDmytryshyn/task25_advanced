﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Task_24_Advanced.DAL.EF;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Interfaces;

namespace Task_24_Advanced.DAL.Repositories
{
    public class BookRepository : IRepository<Book>
    {
        private readonly LibraryContext db;
        public BookRepository(LibraryContext context)
        {
            this.db = context;
        }
        public int Count()
        {
            return db.Books.Count();
        }
        public void Create(Book item)
        {
            db.Books.Add(item);
        }

        public void Delate(int id)
        {
            Book Book = db.Books.Find(id);
            if (Book != null)
            {
                db.Books.Remove(Book);
            }
        }

        public Book GetItem(int id)
        {
            return db.Books.Find(id);
        }

        public IEnumerable<Book> ListItems()
        {
            return db.Books;
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public void Update(Book item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}