﻿using System;
namespace Task_24_Advanced.DAL.Interfaces
{
    public interface IBook : IBaseClass
    {
        string Name { get; set; }
        string ShortDescription { get; set; }
        string Author { get; set; }
        string LinkToImg { get; set; }
        DateTime Date { get; set; }
        string Category { get; set; }
    }
}
