﻿using System;
using Task_24_Advanced.DAL.Entities.Identity;
namespace Task_24_Advanced.DAL.Interfaces.Identity
{
    public interface IClientManager : IDisposable
    {
        void Create(ClientProfile item);
    }
}