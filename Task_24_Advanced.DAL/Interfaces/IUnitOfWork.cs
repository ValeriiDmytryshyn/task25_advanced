﻿using System;
using Task_24_Advanced.DAL.Entities;
using Task_24_Advanced.DAL.Identity;
using Task_24_Advanced.DAL.Interfaces.Identity;

namespace Task_24_Advanced.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Article> Articles { get; }
        IRepository<Book> Books { get; }
        IRepository<Comment> Comments { get; }
        IRepository<Form> Forms { get; }
        ApplicationUserManager UserManager { get; }
        IClientManager ClientManager { get; }
        ApplicationRoleManager RoleManager { get; }
        void Save();
    }
}
