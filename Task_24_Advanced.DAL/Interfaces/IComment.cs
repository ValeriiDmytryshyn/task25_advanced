﻿using System;
namespace Task_24_Advanced.DAL.Interfaces
{
    public interface IComment : IBaseClass
    {
        string Author { get; set; }
        string Content { get; set; }
        DateTime Date { get; set; }
    }
}
