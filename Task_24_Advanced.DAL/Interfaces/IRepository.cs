﻿using System;
using System.Collections.Generic;
namespace Task_24_Advanced.DAL.Interfaces
{
    public interface IRepository<T> : IDisposable
      where T : class
    {
        IEnumerable<T> ListItems();
        T GetItem(int id);
        int Count();
        void Create(T item);
        void Update(T item);
        void Delate(int id);
        void Save();
    }
}
