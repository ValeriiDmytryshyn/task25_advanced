﻿using System.Collections.Generic;
namespace Task_24_Advanced.DAL.Interfaces
{
    public interface IForm : IBaseClass
    {
        string Author { get; set; }
        string Email { get; set; }
        int Age { get; set; }
        string Gender { get; set; }
        List<string> WhichDevice { get; set; }
        List<string> WhichContent { get; set; }
        string Content { get; set; }
    }
}
