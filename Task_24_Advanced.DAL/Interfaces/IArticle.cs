﻿using System;
namespace Task_24_Advanced.DAL.Interfaces
{
    public interface IArticle : IBaseClass
    {
        string Name { get; set; }
        DateTime Date { get; set; }
        string Content { get; set; }
    }
}
