﻿using Microsoft.AspNet.Identity;
using Task_24_Advanced.DAL.Entities.Identity;
namespace Task_24_Advanced.DAL.Identity
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
                : base(store)
        {
        }
    }
}